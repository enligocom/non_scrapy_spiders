from lxml import etree

import re
import csv
import sys
import requests
from os import path

csvDir = sys.argv[1]
resFilePath = path.join(csvDir, 'gorgeousshopcom/gorgeousshopcom.csv')
url = 'http://www.gorgeousshop.com/feeds/GoogleShopping.xml'

r = requests.get(url)
pageTree = etree.fromstring(r.content)
items = pageTree.xpath('//item')
namespace = {'g': 'http://base.google.com/ns/1.0'}

with open(resFilePath, 'w', newline='', encoding='utf-8') as resFile:
    fields = ['Product Name', 'Product ID', 'Category Path', 'Product URL', 'Image URL', 'Delivery Time',
              'Product Price', 'Brand Name', 'Product MPN', 'Product MPN 2', 'Delivery Price']
    dictWriter = csv.DictWriter(resFile, quotechar='"', delimiter=',', fieldnames=fields, quoting=csv.QUOTE_ALL)
    dictWriter.writeheader()
    for item in items:

        def getText(xpath_str, namespace=None):
            res = item.xpath(xpath_str, namespaces=namespace)
            return res[0].text if res else ''

        d = dict()
        d['Product Name'] = getText('title')
        d['Product ID'] = getText('g:id', namespace=namespace)

        path = item.xpath('g:product_type', namespaces=namespace)
        d['Category Path'] = path[-1].text if path else ''

        d['Product URL'] = getText('link')
        d['Image URL'] = getText('g:image_link', namespace=namespace)
        d['Delivery Time'] = getText('g:availability', namespace=namespace)

        salePrice = getText('g:sale_price', namespace=namespace)
        normalPrice = getText('g:price', namespace=namespace)
        finalPrice = re.search(r'[\d.,]+', salePrice or normalPrice)
        d['Product Price'] = finalPrice.group(0).replace(',', '') if finalPrice else ''

        d['Brand Name'] = getText('g:brand', namespace=namespace)
        d['Product MPN'] = getText('g:mpn', namespace=namespace)
        d['Product MPN 2'] = getText('g:gtin', namespace=namespace)

        shippingPrice = getText('g:shipping/g:price', namespace=namespace)
        shippingPrice = re.search(r'[\d.,]+', shippingPrice)
        d['Delivery Price'] = shippingPrice.group(0).replace(',', '') if shippingPrice else ''

        # clean text
        for k in d:
            d[k] = ' '.join(d[k].strip().split())

        dictWriter.writerow(d)
