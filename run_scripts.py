#!/usr/bin/env python3
"""The scripts runs all the python scripts in the directory,
checks the number of strings each of them got as result and sends stats to SCS
Takes 2 mandatory arguments:
1 -- directory with scripts to execute
2 -- feeds root directory path
"""

import os
import sys
import time
import requests
import subprocess
from urllib.parse import urljoin


SCS_URL = 'http://scs:8000/scs/'

scriptsDir = os.path.expanduser(sys.argv[1])
csvFolderPath = os.path.expanduser(sys.argv[2])
os.chdir(scriptsDir)


def processFinishedSpider(spider, timestamp):
    job_stats = dict()
    job_stats['name'] = spider
    job_stats['timestamp'] = timestamp

    try:
        job_stats['amount'] = len(open(os.path.join(csvFolderPath, '{0}/{0}.csv'.format(spider)), 'rb')
                                  .readlines())
    except IOError:
        job_stats['amount'] = 0

    requests.post(urljoin(SCS_URL, 'add_stats'), json=job_stats)


files = [f for f in os.listdir('.') if f.endswith('.py') and os.path.basename(__file__) not in f]
for f in files:
    spiderName = f.split('_py')[0]
    os.makedirs(os.path.join(csvFolderPath, spiderName), exist_ok=True)

    try:
        # FIXME: some scripts are in python 3, another in 2 - port them all to 3, rename files and remove this hack
        # no use python2 - bobiasg
        interpreter = 'python3' # 'python2' if f.endswith('_py2.py') else 'python3'
        subprocess.run([interpreter, f, csvFolderPath], check=True)
    except subprocess.CalledProcessError:
        print('Gotcha!')

    processFinishedSpider(spiderName, time.time())
    time.sleep(5)
