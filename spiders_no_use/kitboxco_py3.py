# from lxml import etree
# import requests
# import csv
# import sys
# import re
# from os import path


# csvDir = sys.argv[1]
# resFilePath = path.join(csvDir, 'kitboxco/kitboxco.csv')
# url = 'https://www.feedoptimise.com/sy/feed/5571e1673064bdc2ac88c4a55e66e6fc.xml?campaign=UK-Shopping-Channels&channel=Affiliatefuture.com'

# r = requests.get(url)
# pageTree = etree.fromstring(r.content)
# items = pageTree.xpath('//product')

# with open(resFilePath, 'w', newline='') as resFile:
#     fields = ['Product ID', 'Product Title', 'Brand Name', 'Product Price', 'Delivery Lead Days',
#               'Category Path', 'Image URL',  'Product URL']
#     dictWriter = csv.DictWriter(resFile, quotechar='"', delimiter=',', fieldnames=fields)
#     dictWriter.writeheader()
#     for item in items:

#         def getText(xpath_str):
#             res = item.xpath(xpath_str)
#             return res[0].text if res else ''

#         d = dict()
#         d['Product ID'] = getText('product_code')
#         d['Product Title'] = getText('product_name')
#         d['Brand Name'] = getText('brand')

#         prodPrice = re.search(r'([\d.,]+)', getText('price'))
#         d['Product Price'] = prodPrice.group(1) if prodPrice else ''

#         d['Delivery Lead Days'] = getText('delivery_lead_days')
#         d['Category Path'] = getText('category')
#         d['Image URL'] = getText('image_url')
#         d['Product URL'] = getText('product_url')

#         dictWriter.writerow(d)
