# from urllib import urlretrieve
# from os import path
# import sys
# import json
# import csv, codecs, cStringIO

# csvDir = path.join(sys.argv[1], 'webgainscom')
# sourceUrl = 'http://api.webgains.com/2.0/vouchers?networks=UK&programs=&joined=1&since=&key=acb67a03222959b99f32ab894b35aa2b'


# class UnicodeWriter:
#     """
#     A CSV writer which will write rows to CSV file "f",
#     which is encoded in the given encoding.
#     """

#     def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
#         # Redirect output to a queue
#         self.queue = cStringIO.StringIO()
#         self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
#         self.stream = f
#         self.encoder = codecs.getincrementalencoder(encoding)()

#     def writerow(self, row):
#         self.writer.writerow([s.encode("utf-8") for s in row])
#         # Fetch UTF-8 output from the queue ...
#         data = self.queue.getvalue()
#         data = data.decode("utf-8")
#         # ... and reencode it into the target encoding
#         data = self.encoder.encode(data)
#         # write to the target stream
#         self.stream.write(data)
#         # empty queue
#         self.queue.truncate(0)

#     def writerows(self, rows):
#         for row in rows:
#             self.writerow(row)

# urlretrieve(sourceUrl, path.join(csvDir, 'source.json'))
# with open(path.join(csvDir, 'source.json'), 'rb') as srcFile:
#     with open(path.join(csvDir, 'webgainscom.csv'), 'wb') as resFile:
#         data = json.load(srcFile)
#         csvWriter = UnicodeWriter(resFile, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL)
#         header = [u'startDate', u'code', u'description', u'currency', u'commissionValue', u'trackingUrl', u'expiryDate', u'discount', u'programId', u'commissionValueFormatted', u'id', u'program_name', u'type', u'commissionType', u'network', u'destinationUrl']
#         csvWriter.writerow(header)

#         for l in data:
#             line = list()
#             for k in header:
#                 col = u' '.join(unicode(l.get(k, '')).split())
#                 if k == u'startDate' or k == u'expiryDate':
#                     col = col.split('T')[0]
#                 line.append(col)
#             csvWriter.writerow(line)
