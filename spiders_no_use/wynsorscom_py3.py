# from lxml import etree

# import re
# import csv
# import sys
# import requests
# from os import path

# csvDir = sys.argv[1]
# resFilePath = path.join(csvDir, 'wynsorscom/wynsorscom.csv')
# url = 'http://feeds.performancehorizon.com/enligo/1100l475/975b4e901f00e61ab5389e256579f1c6.xml'

# r = requests.get(url)
# pageTree = etree.fromstring(r.content)
# items = pageTree.xpath('//item')
# namespace = {'g': 'http://base.google.com/ns/1.0'}

# with open(resFilePath, 'w', newline='', encoding='utf-8') as resFile:
#     fields = ['Product ID', 'Product Name', 'Product Price', 'Delivery Price', 'Delivery Time', 'Brand Name',
#               'Product MPN', 'Product GTIN', 'Product Condition', 'Color Variant', 'Size Variant', 'Gender',
#               'Category Path', 'Image URL', 'Product URL']
#     dictWriter = csv.DictWriter(resFile, quotechar='"', delimiter=',', fieldnames=fields, quoting=csv.QUOTE_ALL)
#     dictWriter.writeheader()


#     def getText(xpath_str, namespace=None):
#         res = item.xpath(xpath_str, namespaces=namespace)
#         return res[0].text if res else ''

#     for item in items:
#         d = dict()
#         d['Product Name'] = getText('title')
#         d['Product ID'] = getText('g:id', namespace=namespace)

#         salePrice = getText('g:sale_price', namespace=namespace)
#         normalPrice = getText('g:price', namespace=namespace)
#         finalPrice = re.search(r'[\d.,]+', salePrice or normalPrice)
#         d['Product Price'] = finalPrice.group(0).replace(',', '') if finalPrice else ''

#         shippingPrice = getText('g:shipping/g:price', namespace=namespace)
#         shippingPrice = re.search(r'[\d.,]+', shippingPrice)
#         d['Delivery Price'] = shippingPrice.group(0).replace(',', '') if shippingPrice else ''

#         d['Delivery Time'] = getText('g:availability', namespace=namespace)
#         d['Brand Name'] = getText('g:brand', namespace=namespace)
#         d['Product MPN'] = getText('g:mpn', namespace=namespace)
#         d['Product GTIN'] = getText('g:gtin', namespace=namespace)
#         d['Product Condition'] = getText('g:condition', namespace=namespace)
#         d['Color Variant'] = getText('g:color', namespace=namespace)
#         d['Size Variant'] = getText('g:size', namespace=namespace)
#         d['Gender'] = getText('g:gender', namespace=namespace)

#         d['Category Path'] = ''
#         paths = item.xpath('g:product_type', namespaces=namespace)
#         if paths:
#             maxPath = (paths[0], len(paths[0].text.split('>')))
#             for p in paths:
#                 pLen = len(p.text.split('>'))
#                 if pLen > maxPath[1]:
#                     maxPath = (p, pLen)
#             d['Category Path'] = maxPath[0].text

#         d['Image URL'] = getText('g:image_link', namespace=namespace)
#         d['Product URL'] = getText('link')

#         # clean text
#         for k in d:
#             d[k] = ' '.join(d[k].strip().split())

#         dictWriter.writerow(d)
