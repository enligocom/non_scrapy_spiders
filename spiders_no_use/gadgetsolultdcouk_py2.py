# import sys
# from os import path
# from lxml import etree
# from urllib2 import urlopen
# import csv, codecs, cStringIO  # UnicodeWriter

# extract_first = lambda i: i[0].text if i else ''

# csvDir = sys.argv[1]
# resFilePath = path.join(csvDir, 'gadgetsolultdcouk/gadgetsolultdcouk.csv')

# url = 'http://gadgetsolultd.co.uk/index.php?route=feed/google_base'
# page = urlopen(url)
# pageTree = etree.parse(page)
# items = pageTree.xpath('//item')
# namespace = {'g': 'http://base.google.com/ns/1.0'}


# class UnicodeWriter:
#     """
#     A CSV writer which will write rows to CSV file "f",
#     which is encoded in the given encoding.
#     """

#     def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
#         # Redirect output to a queue
#         self.queue = cStringIO.StringIO()
#         self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
#         self.stream = f
#         self.encoder = codecs.getincrementalencoder(encoding)()

#     def writerow(self, row):
#         self.writer.writerow([s.encode("utf-8") for s in row])
#         # Fetch UTF-8 output from the queue ...
#         data = self.queue.getvalue()
#         data = data.decode("utf-8")
#         # ... and reencode it into the target encoding
#         data = self.encoder.encode(data)
#         # write to the target stream
#         self.stream.write(data)
#         # empty queue
#         self.queue.truncate(0)

#     def writerows(self, rows):
#         for row in rows:
#             self.writerow(row)


# with open(resFilePath, 'wb') as resFile:
#     fields = ['Product Name', 'Brand Name', 'Product ID', 'Image URL', 'Product MPN', 'Product MPN 2',
#               'Product MPN 3', 'Product Price', 'Category Path', 'Delivery Time', 'Product URL']

#     resCsv = UnicodeWriter(resFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
#     resCsv.writerow(fields)
#     for item in items:
#         res = list()
#         res.append(extract_first(item.xpath('./title')))
#         res.append(extract_first(item.xpath('./g:brand', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:id', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:image_link', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:mpn', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:upc', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:ean', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:price', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:product_type', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./g:quantity', namespaces=namespace)))
#         res.append(extract_first(item.xpath('./link')))

#         res = [col.replace('\n', ' ').replace('\r\n', ' ') if col else '' for col in res]
#         resCsv.writerow(res)
