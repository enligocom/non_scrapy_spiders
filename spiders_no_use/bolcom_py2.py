# import threading
# import urllib2
# import os
# import sys
# import gzip
# import shutil
# import csv

# pathToUrls = 'bolcom_feeds.txt'
# pathToResDir = os.path.join(sys.argv[1], 'bolcom')
# pathToResFile = os.path.join(pathToResDir, 'bolcom.csv')
# pathToDiscardedFile = os.path.join(pathToResDir, 'bolcom_discarded.csv')

# for f in os.listdir(pathToResDir):
#     if '.csv' in f:
#         os.remove(os.path.join(pathToResDir, f))


# def loadAndSave(url):
#     r = urllib2.Request(url)
#     r = urllib2.urlopen(r)

#     comprFileName = os.path.join(pathToResDir, url.split('/')[-1].strip())
#     with open(comprFileName, 'wb') as oFile:
#         oFile.write(r.read())
#         oFile.flush()

#     # decompress file
#     with gzip.open(comprFileName, 'rb') as comprFile, open(comprFileName.split('.gz')[0], 'wb') as resFile:
#         shutil.copyfileobj(comprFile, resFile)
#         resFile.flush()


# def shouldDiscardRow(row):
#     if len(row) < 13:
#         return True

#     # OfferNL.isDeliverable: *delete if has blank entries*
#     a = row[12].strip() == ''
#     # title: delete if includes the following content: "promo 2 stuks" "multi bundel"
#     title = row[2].lower()
#     b = 'promo 2 stuks' in title or 'multi bundel' in title
#     # brand: delete if include the following content: "merkloos"
#     brand = row[7].lower()
#     c = 'merkloos' in brand

#     return a or b or c

# # read urls from file
# urls = list()
# with open(pathToUrls) as urlsFile:
#     for line in urlsFile:
#         urls.append(line.strip())

# # create threads and run them
# threadPool = list()
# for url in urls:
#     threadPool.append(threading.Thread(target=(lambda i=url: loadAndSave(i))))

# for thread in threadPool:
#     thread.start()

# for thread in threadPool:
#     thread.join()

# # FINISH HIM!!! hmmm... I mean, save the result and remove temporary files
# os.chdir(pathToResDir)
# tempFiles = [f for f in os.listdir(pathToResDir) if f.endswith('.csv')]
# fileWithHeader = ''
# for f in tempFiles:
#     with open(f) as temp:
#         if not temp.readline().startswith('""'):
#             fileWithHeader = f
#             break

# if not fileWithHeader:
#     exit(-1)

# # I, Q, S, AA, AC, AG, AI, AK, AL, AM, AN, AO, AQ, AR, AW, AX, AT, AY, AZ, BA, BB, BC, BD, BE, BF, BG, BH, BI, BJ
# columnNums = [8, 16, 18, 26, 28, 32, 34, 36, 37, 38, 39, 40, 42, 43, 48, 49, 45, 50, 51, 52, 53, 54, 55, 56, 57, 58,
#               59, 60, 61]
# with open(pathToResFile, 'wb') as resFile, open(pathToDiscardedFile, 'wb') as discarded:
#     resWriter = csv.writer(resFile, delimiter='|', quotechar='"', quoting=csv.QUOTE_ALL)
#     discardedWriter = csv.writer(discarded, delimiter='|', quotechar='"', quoting=csv.QUOTE_ALL)
#     for f in tempFiles:
#         with open(f, 'rb') as inpFile:
#             csvReader = csv.reader(inpFile, delimiter='|', quotechar='"', quoting=csv.QUOTE_ALL)
#             isFirstLine = True
#             for row in csvReader:
#                 if f != fileWithHeader and isFirstLine:
#                     isFirstLine = False
#                     continue

#                 row = (row[:8] + row[9:16] + row[17:18] + row[19:26] + row[27:28] + row[29:32] + row[33:34] + row[35:36]
#                        + row[41:42] + row[44:45] + row[46:48] + row[62:])
#                 if shouldDiscardRow(row):
#                     discardedWriter.writerow(row)
#                 else:
#                     resWriter.writerow(row)
#         os.remove(f)
