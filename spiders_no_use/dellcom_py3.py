# from lxml import etree
# import requests
# import csv
# import sys
# from os import path
# from urllib.parse import unquote

# csvRootDir = sys.argv[1]
# resFilePath = path.join(csvRootDir, 'dellcom/dellcom.csv')
# url = 'http://feeds.performancehorizon.com/enligo/1100l318/01b110603110ce7bb63d85e5c967c5ba'

# r = requests.get(url)
# pageTree = etree.fromstring(r.content)
# items = pageTree.xpath('//Product')

# with open(resFilePath, 'w', newline='') as resFile:
#     fields = ['Product ID', 'Product Title', 'Product Price', 'Delivery Price', 'Delivery Time', 'Product SKU',
#               'Category Path', 'Image URL',  'Product URL']
#     dictWriter = csv.DictWriter(resFile, quotechar='"', delimiter=',', fieldnames=fields)
#     dictWriter.writeheader()
#     for item in items:

#         def getText(xpath_str):
#             res = item.xpath(xpath_str)
#             return res[0].text if res else ''

#         d = dict()
#         d['Product ID'] = getText('Model_id')
#         d['Product Title'] = unquote(getText('Model_name'))
#         d['Product Price'] = getText('Price_now')
#         d['Delivery Price'] = getText('ShippingCosts')
#         d['Delivery Time'] = getText('ShippingDate')
#         d['Product SKU'] = getText('SKU')
#         d['Category Path'] = getText('Category')
#         d['Image URL'] = getText('Hero_Image')
#         d['Product URL'] = getText('URL')

#         dictWriter.writerow(d)
