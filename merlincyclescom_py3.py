from lxml import etree
import requests
import csv
import sys
import re
from os import path

csvDir = sys.argv[1]
resFilePath = path.join(csvDir, 'merlincyclescom/merlincyclescom.csv')
url = 'http://feeds.performancehorizon.com/enligo/1101l375/bbb288e6e5e5fdf80514341750776e51.xml'

r = requests.get(url)
pageTree = etree.fromstring(r.content)
items = pageTree.xpath('//merchant/product')

with open(resFilePath, 'w', newline='') as resFile:
    fields = ['Product ID', 'Product Title', 'Color Variant', 'Product Price', 'Brand Name',  'Delivery Time',
              'Availability', 'Product EAN', 'Product MPN', 'Category Path', 'Image URL', 'Product URL', 'Currency']
    dictWriter = csv.DictWriter(resFile, quotechar='"', delimiter=',', fieldnames=fields, quoting=csv.QUOTE_ALL)
    dictWriter.writeheader()


    def getText(xpath_str):
        res = item.xpath(xpath_str)
        return res[0].text if res else ''

    for item in items:
        d = dict()
        d['Product ID'] = getText('pid')
        d['Product Title'] = getText('name')
        d['Color Variant'] = getText('colour')
        d['Product Price'] = getText('price/actualp').replace(',', '')
        d['Brand Name'] = getText('brand')
        d['Delivery Time'] = getText('deltime')

        stock = item.xpath('./@instock')
        d['Availability'] = 'InStock' if stock and stock[0] == 'yes' else 'OutOfStock'

        d['Product EAN'] = getText('ean')
        d['Product MPN'] = getText('mpn')
        d['Category Path'] = getText('category')
        d['Image URL'] = getText('imgurl')

        url = re.search(r'/destination:(.+)', getText('purl'))
        if not url:
            continue
        d['Product URL'] = url.group(1)
        d['Currency'] = getText('currency')

        dictWriter.writerow(d)
